﻿using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)

        {
            string input1 = Console.ReadLine();
            string input2 = Console.ReadLine();
            int number1 = 0, number2 = 0;
            Int32.TryParse(input1, out number1);
            Int32.TryParse(input2, out number2);
            Enter(number1, number2);
        }
        public static int Summ(int a, int b)
        {
            int result = a + b;
            return result;
        }
        public static int Sub(int a, int b)
        {
            int result = a - b;
            return result;
        }
        public static int Multply(int a, int b)
        {
            int result = a * b;
            return result;
        }
        public static int Div(int a, int b)
        {
            if (b == 0)
            {
                Console.WriteLine("Сударь, вы не Илон Маск");
                return 0;
            }
            else
            {
                int result = a / b;
                return result;
            }
        }     

        public static void Enter(int a, int b)
        {
            Console.WriteLine($"{a} + {b} = {Summ(a, b)}");
            Console.WriteLine($"{a} - {b} = {Sub(a, b)}");
            Console.WriteLine($"{a} * {b} = {Multply(a, b)}");
            Console.WriteLine($"{a} / {b} = {Div(a, b)}");
        }
    }
}
